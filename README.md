# City Generator 2: Starter Pack

The City Generator 2: Starter Pack is a collection of input data sets ready for
use with `city_generator2`. It also provides a POSIX shell script that make
easier the post-processing of output data. It gives an overview of the required
input data and the features of `city_generator2`.

## Installation from sources

City Generator 2 can be built directly from its source tree. The simplest way to
build City Generator 2 from its sources is to rely on the city\_generator branch
of the
[Star-Engine](https://gitlab.com/meso-star/star-engine/tree/city_generator)
project: it provides CMake scripts that automate the download, the compilation
and the installation of City Generator 2. This build procedure assumes the
following prerequisites:

- [git](https://git-scm.com/) source control.
- [GNU Compiler Collection](https://gcc.gnu.org/) in version 8.1 or higher.
- [CMake](https://cmake.org/) in version 3 or higher.
- Optionally, the [AsciiDoc](https://asciidoc.org/) tool suite to generate the
  man pages of the reference documentation.

### Build

Assuming that the aforementioned prerequisites are available, the build
procedure is summed up to:

~ $ git clone -b city\_generator https://gitlab.com/meso-star/star-engine.git city\_generator2  
~ $ mkdir city\_generator2/build; cd city\_generator2/build  
~/city\_generator2/build $ cmake ../cmake  
~/city\_generator2/build $ make 

By default City Generator 2 is installed in the local subdirectory of
city\_generator2. As the last step of the installation, we need to generate the
`city_generator2.profile` POSIX shell script that will be run in a running shell
to update its environment variables with the `city_generator2` installation
directory.

$ sh ~/city\_generator2/local/etc/build-city\_generator2-profile.sh

### Run

Before running `city_generator2`, you must first run the
`city_generator2.profile` script once in the current POSIX shell to update its
environment variables. Then you can run the stardis program and check out its
reference documentation.

$ . ~/city\_generator2/local/etc/city\_generator2.profile  
$ city\_generator2 -h 

We point out that the `city_generator2.profile` script must be run in each new
POSIX shell in which you want to give access to the City Generator
2 installation.

## A first example

City Generator 2 has been designed to create input data from databases like
[Open Street Map](https://www.openstreetmap.org/) or [BD
TOPO](https://geoservices.ign.fr/bdtopo) or from programs like [Procedural
Generator 2](https://gitlab.com/meso-star/pg2). Thus the input data consists of
a city map and building data catalogs:

- the file city.yaml describes the city map. The buildings are only described in
  "2,5 D" i.e. a ground contour and a height as well as a construction mode and
the reference of the data set corresponding to this mode;
- data "catalogs". There is one catalog per construction mode (catalog0.yaml and
  catalog1.yaml in this example).

The construction mode is the way to generate the 3D from a simple ground contour
and the height. For the moment there are 2 construction modes implemented. And
then we will look for the data such as the wall thickness in the data catalogs. 

You can launch `city_generator2` with the provided dataset. 

$ city\_generator2 -m city.yaml -c catalog0.yaml -c catalog1.yaml -a

This will produce a collection of stl files according to a certain nomenclature.
There will be, for example, one stl file per building describing the walls. And
files depending on the construction mode and the data catalog like internal or
external insulation.

The `-a` option indicates to produce the stl files in ascii format. By default
they are binary.

For more informations about the input data and output data, you refer to the
corresponding man page city\_generator2-input and city\_generator2-output.

## Make a model for Stardis

All of these stl files describe a conformal mesh of the city as required as the
[Stardis](https://meso-star.com/projects/stardis/stardis.html) thermal solver.

We also provide a POSIX shell script that produces an input file for Stardis. To
reduce the length of the file, it groups together by concatenation stl files
(that'why we have produced ascii stl files), elements similar to all buildings
such as walls for example.

$ sh make\_stardis\_model.sh > model.txt

This script should be considered as a proposal to chain City Generator 2 and
Stardis.

## Release notes


## Copyright notice

Copyright (C) 2023 Université de Pau et des Pays de l'Adour UPPA.  
Copyright (C) 2023 CNRS  
Copyright (C) 2023 Sorbonne Université  
Copyright (C) 2023 Université Paul Sabatier  
Copyright (C) 2023 [|Méso|Star>](https://www.meso-star.com) (<contact@meso-star.com>).

## License

City Generator 2: Starter Pack is released under the GPLv3+ license: GNU GPL
version 3 or later. You can freely study, modify or extend it. You are also
welcome to redistribute it under certain conditions; refer to the
[license](https://www.gnu.org/licenses/gpl.html) for details.
