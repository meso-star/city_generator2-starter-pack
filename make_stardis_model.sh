#!/bin/sh

# Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 


###################
# utility functions
###################

concat_stl()
{
  printf "%s \n" "solid name"
  for i in "$@" ; do
    sed '/solid/d' "${i}"
  done
  printf "%s \n" "endsolid name"
}

print_ground()
{
  printf "SOLID %-16s %-7s %-7s %-7s AUTO    %-7s UNKNOWN 0       FRONT GROUND_B.stl FRONT %s\n" "${medium_name}" "${lambda}" "${rho}" "${cp}" "${Tinit}" "${medium_name}.stl"
}

print_solid()
{
  printf "SOLID %-16s %-7s %-7s %-7s AUTO    %-7s UNKNOWN 0       FRONT %s\n" "${medium_name}" "${lambda}" "${rho}" "${cp}" "${Tinit}" "${medium_name}.stl"
}

print_fluid()
{
  printf "FLUID %-16s %-6s %-6s %-4s UNKNOWN  FRONT %s\n" "${medium_name}" "${rho}" "${cp}" "${Tinit}" "${medium_name}.stl"
}

print_boundary_H()
{
  printf "H_BOUNDARY_FOR_SOLID %-16s %-4s %-4s %-4s %-4s %-4s %s\n" "${boundary_name}" "${Tref}" "${emissivity}" "${specular_fraction}" "${H}" "${Text}" "${boundary_name}.stl"
}

print_boundary_T()
{
  printf "T_BOUNDARY_FOR_SOLID %-16s %-4s %-4s\n" "${boundary_name}" "${T}" "${boundary_name}.stl"
}

print_connection()
{
  printf "SOLID_FLUID_CONNECTION %-16s %-4s %-4s %-4s %-4s %s\n" "${connection_name}" "${Tref}" "${emissivity}" "${specular_fraction}" "${H}" "${connection_name}.stl"
}

process_ground()
{
  concat_stl ground_B_*.stl > GROUND_B.stl  
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${medium_name}".stl  
    print_ground
  fi
}

process_solid()
{
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${medium_name}".stl  
    print_solid
  fi
}

process_fluid()
{
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${medium_name}".stl  
    print_fluid
  fi
}

process_boundary_H()
{
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${boundary_name}".stl  
    print_boundary_H
  fi
}

process_boundary_T()
{
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${boundary_name}".stl  
    print_boundary_T
  fi
}

process_connection()
{
  files=$(find ./${pattern}  -prune 2> /dev/null | wc -l)
  if [ "$files" != 0 ]; then
    concat_stl ./${pattern} > "${connection_name}".stl  
  fi
  print_connection
}

################################################################################
################################################################################
################################################################################

printf "#THE GROUND\n"
printf "#     NAME             lambda  rho     cp      delta   Tinit   T       Power   triangles\n"
medium_name=S_GROUND
pattern="*_C_ground*.stl"
lambda=2
rho=2000
cp=1000
Tinit=273
process_ground

printf "\n#LIST OF SOLIDS\n"
printf "#     NAME             lambda  rho     cp      delta   Tinit   T       Power   triangles\n"

#EXTERNAL_INSULATION
medium_name=S_EXT_INS
pattern="*_S_external_insulation*.stl"
lambda=0.05
rho=18
cp=1000
Tinit=273
process_solid

#FLOOR
medium_name=S_FLOOR
pattern="*_S_floor.stl"
lambda=1.5
rho=2300
cp=2500
Tinit=273
process_solid

#FLOOR_INSULATION
medium_name=S_FLOOR_INS
pattern="*_S_floor_insulation*.stl"
lambda=0.05
rho=18
cp=1000
Tinit=273
process_solid

#FOUNDATION
medium_name=S_FOUNDATION
pattern="*_S_foundation*.stl"
lambda=1.5
rho=2300
cp=2500
Tinit=273
process_solid

#GLASS
medium_name=S_GLAZING
pattern="*_S_glazing*.stl"
lambda=1
rho=2500
cp=720
Tinit=273
process_solid

#INTER_FLOOR
medium_name=S_INTER_FLOOR
pattern="*_S_intermediate_floors*.stl"
lambda=1.5
rho=2300
cp=2500
Tinit=273
process_solid

#INTERNAL_INSULATION
medium_name=S_INT_INS
pattern="*_S_internal_insulation*.stl"
lambda=0.05
rho=18
cp=1000
Tinit=273
process_solid

#ROOF
medium_name=S_ROOF
pattern="*_S_roof.stl"
lambda=1.5
rho=2300
cp=2500
Tinit=273
process_solid

#ROOF_INSULATION
medium_name=S_ROOF_INS
pattern="*_S_roof_insulation*.stl"
lambda=0.05
rho=18
cp=1000
Tinit=273
process_solid

#WALL
medium_name=S_WALL
pattern="*_S_walls.stl"
lambda=0.4
rho=650
cp=1000
Tinit=273
process_solid

printf "\n#LIST OF FLUIDS\n"
#ATTIC
medium_name=F_ATTIC
pattern="*_F_attic*.stl"
rho=1.25
cp=1000
Tinit=273
process_fluid

#HABITABLE CAVITY
medium_name=F_CAVITY
pattern="*_F_internal.stl *_F_levels.stl"
rho=1.25
cp=1000
Tinit=273
process_fluid

#CRAWLSPACE
medium_name=F_CRAWL
pattern="*_F_crawlspace*.stl"
rho=1.25
cp=1000
Tinit=273
process_fluid

printf "\n#LIST OF BOUNDARIES\n"
#BUILDING BOUNDARIES
boundary_name=B_BUILDING
pattern="bat*_B_*.stl"
Tref=273
emissivity=0.9
specular_fraction=0
H=10
Text=273
process_boundary_H

#GROUND BOUNDARY
boundary_name=B_GROUND_LAT
pattern="ground_B_lateral_*.stl"
Tref=273
emissivity=0.9
specular_fraction=0
H=0
Text=273
process_boundary_H

boundary_name=B_GROUND_TOP
pattern="ground_B_top.stl"
Tref=273
emissivity=0.9
specular_fraction=0
H=10
Text=273
process_boundary_H

boundary_name=B_GROUND_BOTTOM
pattern="ground_B_bottom.stl"
T=286
process_boundary_T

printf "\n#LIST OF CONNECTIONS\n"
connection_name=C_CAVITY_BUILDING
pattern="*_C_internal_*.stl *_C_levels_*.stl *_C_attic*.stl *_C_crawlspace*.stl"
Tref=293
emissivity=0.9
specular_fraction=0
H=5
process_connection
